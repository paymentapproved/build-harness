# build-harness

an experimental project to facilitate sharing tools across various projects

## Usage

To include the build harness into your project creat a Makefile in your project root with the following content

```Makefile
-include $(shell curl -sSL -o .build-harness "https://gitlab.com/paymentapproved/build-harness/-/raw/main/templates/Makefile.build-harness"; echo .build-harness)
```

to initialize the build harness execute the command

```sh
make init
```

update the build harness by pulling in upstream changes

```sh
make refresh-build-harness
```

to view list of available targets

```sh
make help/all
```
