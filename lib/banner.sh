#!/usr/bin/env bash

export BANNER='
########     ###    ##    ## ##     ## ######## ##    ## ########    ###    ########  ########  ########   #######  ##     ## ######## ########  
##     ##   ## ##    ##  ##  ###   ### ##       ###   ##    ##      ## ##   ##     ## ##     ## ##     ## ##     ## ##     ## ##       ##     ## 
##     ##  ##   ##    ####   #### #### ##       ####  ##    ##     ##   ##  ##     ## ##     ## ##     ## ##     ## ##     ## ##       ##     ## 
########  ##     ##    ##    ## ### ## ######   ## ## ##    ##    ##     ## ########  ########  ########  ##     ## ##     ## ######   ##     ## 
##        #########    ##    ##     ## ##       ##  ####    ##    ######### ##        ##        ##   ##   ##     ##  ##   ##  ##       ##     ## 
##        ##     ##    ##    ##     ## ##       ##   ###    ##    ##     ## ##        ##        ##    ##  ##     ##   ## ##   ##       ##     ## 
##        ##     ##    ##    ##     ## ######## ##    ##    ##    ##     ## ##        ##        ##     ##  #######     ###    ######## ########  
  '
